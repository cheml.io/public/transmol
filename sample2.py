

import moses

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import math, copy, time
from torch.autograd import Variable


"""# Model

## Model Architecture
"""

class EncoderDecoder(nn.Module):
    """
    A standard Encoder-Decoder architecture. Base for this and many 
    other models.
    """
    def __init__(self, encoder, decoder, src_embed, tgt_embed, generator):
        super(EncoderDecoder, self).__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.src_embed = src_embed
        self.tgt_embed = tgt_embed
        self.generator = generator
        
    def forward(self, src, tgt, src_mask, tgt_mask):
        "Take in and process masked src and target sequences."
        return self.decode(self.encode(src, src_mask), src_mask,
                            tgt, tgt_mask)
    
    def encode(self, src, src_mask, gaussian_mean=None, gaussian_std=None):
      if gaussian_mean is None:
        return self.encoder(self.src_embed(src), src_mask)
      else:
        output = self.encoder(self.src_embed(src), src_mask)
        with torch.no_grad():
          noise = torch.zeros_like(output).normal_(gaussian_mean, gaussian_std)
          return output.add(noise)
    
    def decode(self, memory, src_mask, tgt, tgt_mask):
        return self.decoder(self.tgt_embed(tgt), memory, src_mask, tgt_mask)

class Generator(nn.Module):
    "Define standard linear + softmax generation step."
    def __init__(self, d_model, vocab):
        super(Generator, self).__init__()
        self.proj = nn.Linear(d_model, vocab)

    def forward(self, x, temperature=None):
        logits = self.proj(x)
        if temperature == None:
          return F.log_softmax(logits, dim=-1)
        else:
          return F.log_softmax(logits.div(temperature), dim=-1)

"""## Encoder and Decoder Stacks"""

def clones(module, N):
    "Produce N identical layers."
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])

class Encoder(nn.Module):
    "Core encoder is a stack of N layers"
    def __init__(self, layer, N):
        super(Encoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(layer.size)
        
    def forward(self, x, mask):
        "Pass the input (and mask) through each layer in turn."
        for layer in self.layers:
            x = layer(x, mask)
        return self.norm(x)    
    
class LayerNorm(nn.Module):
    "Construct a layernorm module (See citation for details)."
    def __init__(self, features, eps=1e-6):
        super(LayerNorm, self).__init__()
        self.a_2 = nn.Parameter(torch.ones(features))
        self.b_2 = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.a_2 * (x - mean) / (std + self.eps) + self.b_2
    
class SublayerConnection(nn.Module):
    """
    A residual connection followed by a layer norm.
    Note for code simplicity the norm is first as opposed to last.
    """
    def __init__(self, size, dropout):
        super(SublayerConnection, self).__init__()
        self.norm = LayerNorm(size)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x, sublayer):
        "Apply residual connection to any sublayer with the same size."
        return x + self.dropout(sublayer(self.norm(x)))

class EncoderLayer(nn.Module):
    "Encoder is made up of self-attn and feed forward (defined below)"
    def __init__(self, size, self_attn, feed_forward, dropout):
        super(EncoderLayer, self).__init__()
        self.self_attn = self_attn
        self.feed_forward = feed_forward
        self.sublayer = clones(SublayerConnection(size, dropout), 2)
        self.size = size

    def forward(self, x, mask):
        "Follow Figure 1 (left) for connections."
        x = self.sublayer[0](x, lambda x: self.self_attn(x, x, x, mask))
        return self.sublayer[1](x, self.feed_forward)

class Decoder(nn.Module):
    "Generic N layer decoder with masking."
    def __init__(self, layer, N):
        super(Decoder, self).__init__()
        self.layers = clones(layer, N)
        self.norm = LayerNorm(layer.size)
        
    def forward(self, x, memory, src_mask, tgt_mask):
        for layer in self.layers:
            x = layer(x, memory, src_mask, tgt_mask)
        return self.norm(x)

class DecoderLayer(nn.Module):
    "Decoder is made of self-attn, src-attn, and feed forward (defined below)"
    def __init__(self, size, self_attn, src_attn, feed_forward, dropout):
        super(DecoderLayer, self).__init__()
        self.size = size
        self.self_attn = self_attn
        self.src_attn = src_attn
        self.feed_forward = feed_forward
        self.sublayer = clones(SublayerConnection(size, dropout), 3)
 
    def forward(self, x, memory, src_mask, tgt_mask):
        "Follow Figure 1 (right) for connections."
        m = memory
        x = self.sublayer[0](x, lambda x: self.self_attn(x, x, x, tgt_mask))
        x = self.sublayer[1](x, lambda x: self.src_attn(x, m, m, src_mask))
        return self.sublayer[2](x, self.feed_forward)

def subsequent_mask(size):
    "Mask out subsequent positions."
    attn_shape = (1, size, size)
    subsequent_mask = np.triu(np.ones(attn_shape), k=1).astype('uint8')
    return torch.from_numpy(subsequent_mask) == 0

def attention(query, key, value, mask=None, dropout=None):
    "Compute 'Scaled Dot Product Attention'"
    d_k = query.size(-1)
    # print(d_k)
    # print(query.size())
    # print(key.transpose(-2, -1).size())
    scores = torch.matmul(query, key.transpose(-2, -1)) \
             / math.sqrt(d_k)
    if mask is not None:
        scores = scores.masked_fill(mask == 0, -1e9)
    p_attn = F.softmax(scores, dim = -1)
    if dropout is not None:
        p_attn = dropout(p_attn)
    return torch.matmul(p_attn, value), p_attn

class MultiHeadedAttention(nn.Module):
    def __init__(self, h, d_model, dropout=0.1):
        "Take in model size and number of heads."
        super(MultiHeadedAttention, self).__init__()
        assert d_model % h == 0
        # We assume d_v always equals d_k
        self.d_k = d_model // h
        self.h = h
        self.linears = clones(nn.Linear(d_model, d_model), 4)
        self.attn = None
        self.dropout = nn.Dropout(p=dropout)
        
    def forward(self, query, key, value, mask=None):
        "Implements Figure 2"
        if mask is not None:
            # Same mask applied to all h heads.
            mask = mask.unsqueeze(1)
        nbatches = query.size(0)
        
        # 1) Do all the linear projections in batch from d_model => h x d_k 
        query, key, value = \
            [l(x).view(nbatches, -1, self.h, self.d_k).transpose(1, 2)
             for l, x in zip(self.linears, (query, key, value))]
        
        # 2) Apply attention on all the projected vectors in batch. 
        x, self.attn = attention(query, key, value, mask=mask, 
                                 dropout=self.dropout)
        # 3) "Concat" using a view and apply a final linear. 
        x = x.transpose(1, 2).contiguous() \
             .view(nbatches, -1, self.h * self.d_k)
        return self.linears[-1](x)

"""## Position-wise Feed-Forward Networks"""

class PositionwiseFeedForward(nn.Module):
    "Implements FFN equation."
    def __init__(self, d_model, d_ff, dropout=0.1):
        super(PositionwiseFeedForward, self).__init__()
        self.w_1 = nn.Linear(d_model, d_ff)
        self.w_2 = nn.Linear(d_ff, d_model)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        return self.w_2(self.dropout(F.relu(self.w_1(x))))

"""## Embeddings and Softmax"""

class Embeddings(nn.Module):
    def __init__(self, d_model, vocab):
        super(Embeddings, self).__init__()
        self.lut = nn.Embedding(vocab, d_model)
        self.d_model = d_model

    def forward(self, x):
        return self.lut(x) * math.sqrt(self.d_model)

"""## Positional Encoding"""

class PositionalEncoding(nn.Module):
    "Implement the PE function."
    def __init__(self, d_model, dropout, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        
        # Compute the positional encodings once in log space.
        pe = torch.zeros(max_len, d_model)
        
        
        position = torch.arange(0., max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0., d_model, 2) *
                             -(math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0)
        self.register_buffer('pe', pe)
        
    def forward(self, x):
        x = x + Variable(self.pe[:, :x.size(1)], 
                         requires_grad=False)
        return self.dropout(x)

"""## Full Model"""

def make_model(src_vocab, tgt_vocab, N=6, 
               d_model=512, d_ff=2048, h=8, dropout=0.1):
    "Helper: Construct a model from hyperparameters."
    c = copy.deepcopy
    attn = MultiHeadedAttention(h, d_model)
    ff = PositionwiseFeedForward(d_model, d_ff, dropout)
    position = PositionalEncoding(d_model, dropout)
    model = EncoderDecoder(
        Encoder(EncoderLayer(d_model, c(attn), c(ff), dropout), N),
        Decoder(DecoderLayer(d_model, c(attn), c(attn), 
                             c(ff), dropout), N),
        nn.Sequential(Embeddings(d_model, src_vocab), c(position)),
        nn.Sequential(Embeddings(d_model, tgt_vocab), c(position)),
        Generator(d_model, tgt_vocab))
    

    for p in model.parameters():
        if p.dim() > 1:
            nn.init.xavier_uniform(p)
    return model

"""## Vocab loader"""

def tokenize_smiles(text):
  return list(text)

from torchtext import data

if True:
  AUGEMENTED_PATH = 'data/aug'

  BOS_WORD = '<s>'
  EOS_WORD = '</s>'
  BLANK_WORD = "<blank>"

  SRC = torch.load(AUGEMENTED_PATH+"/src")
  TGT = torch.load(AUGEMENTED_PATH+"/tgt")


"""## Beam Decode"""

import operator
import torch
import torch.nn as nn
import torch.nn.functional as F
from queue import PriorityQueue
from functools import total_ordering

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
SOS_token = TGT.vocab.stoi["<s>"]
EOS_token = TGT.vocab.stoi["</s>"]
MAX_LENGTH = 50


@total_ordering
class BeamSearchNode(object):
    def __init__(self, hiddenstate, previousNode, wordId, logProb, length, reward=1):
        '''
        :param hiddenstate:
        :param previousNode:
        :param wordId:
        :param logProb:
        :param length:
        '''
        self.h = hiddenstate
        self.prevNode = previousNode
        self.wordid = wordId
        self.logp = logProb
        self.leng = length
        self.slogp = None
        self.reward = reward

    def eval(self, alpha=0.75):
        
        if self.slogp == None:
          self.slogp = self.get_slogp()

        # return self.logp / float(self.leng - 1 + 1e-6) + alpha * reward
        return self.slogp/(self.leng/self.reward)**alpha

    def get_slogp(self):
        if self.prevNode == None:
          return self.logp
        return self.logp+self.prevNode.get_slogp()


    def __gt__(self, other):
      return False


def beam_decode(model=None, src=None, src_mask=None, max_len=None, start_symbol=None, stochastic=False, memory=None, gaussian_mean=None, 
                gaussian_std=None, temperature=None, give_up_limit = 2000, topk=10, beam_width=10, length_reward=1.0):
    '''
    :param length_reward: the value which is if >1 then, greater punishes long strings, if <1 encorouges longer strings
    :param target_tensor: target indexes tensor of shape [B, T] where B is the batch size and T is the maximum length of the output sentence
    :param decoder_hidden: input tensor of shape [1, B, H] for start of the decoding
    :param encoder_outputs: if you are using attention mechanism you can pass encoder outputs, [T, B, H] where T is the maximum length of input sentence
    :return: decoded_batch
    '''

    # beam_width = 10
    # topk = 10  # how many sentence do you want to generate
    decoded_batch = []
    if memory is None:
      memory = model.encode(src, src_mask, gaussian_mean, gaussian_std)
    ys = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)

    # decoding goes sentence by sentence
    for idx in range(1):

        # Start with the start of the sentence token
        decoder_input = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
        decoder_init = torch.ones(1, 1).fill_(start_symbol).type_as(src.data)
        # Number of sentence to generate
        endnodes = []
        number_required = min((topk + 1), topk - len(endnodes))

        # starting node -  hidden vector, previous node, word id, logp, length
        node = BeamSearchNode(memory, None, decoder_input, 0, 1)
        nodes = PriorityQueue()

        # start the queue
        nodes.put((-node.eval(), node))
        qsize = 1
        # print(node)

        # start beam search
        while True:
            # print("*")
            # print(qsize)
            # print(nodes)
            # give up when decoding takes too long
            if qsize > give_up_limit: break

            # fetch the best node
            if nodes.qsize() < 1:
              break
            score, n = nodes.get()
            # print("Best node")number_required
            # print((score, n))
            # print("Previous word of best node:" + str(n.prevNode))
            decoder_input = []
            decoder_input.append(n.wordid)
            # print("While loop")
            prN = n
            # print(prN.prevNode != None)
            while prN.prevNode != None:
                prN = prN.prevNode
                # print(prN)
                decoder_input.append(prN.wordid)
            # print("While loop end")
            decoder_input = decoder_input[::-1]
            # decoder_input = n.wordid
            temp = decoder_init
            # print(decoder_input)
            for word in decoder_input[1:]:
              # print("Decoder input loop works")
              temp = torch.cat([temp, 
                        torch.ones(1, 1).type_as(src.data).fill_(word)], dim=1)
            # print("Input:")
            # print(temp)
            decoder_input = temp
            decoder_hidden = n.h

            if n.wordid.item() == EOS_token and n.prevNode != None:
                endnodes.append((score, n))
                # if we reached maximum # of sentences required
                # print("*"*110)
                # print("We have reached")
                if len(endnodes) >= number_required:
                    break
                else:
                    continue

            # decode for one step using decoder
            decoder_output = model.decode(memory, src_mask, 
                           Variable(decoder_input), 
                           Variable(subsequent_mask(decoder_input.size(1))
                                    .type_as(src.data))) #decoder(decoder_input, decoder_hidden, encoder_output)
            
            decoder_output = model.generator(decoder_output[:, -1], temperature)


            # _, next_word = torch.max(decoder_output, dim = 1)
            
            # next_word = next_word.data[0]
            # print("Greedy output next word")
            # print(decoder_output)

            # PUT HERE REAL BEAM SEARCH OF TOP
            if not stochastic:
              log_prob, indexes = torch.topk(decoder_output, beam_width)
              # print(log_prob)
              # print(indexes)
            else:
              dist = torch.distributions.categorical.Categorical(logits=decoder_output[0])
              indexes = torch.unique(dist.sample((beam_width,))).unsqueeze(dim=0)
              log_prob = decoder_output[0][indexes]
              # print(log_prob)
              # print(indexes)

            nextnodes = []
            
            # import pdb; pdb.set_trace()
            

            for new_k in range(indexes.size(1)):
                decoded_t = indexes[0][new_k].view(1, -1)[0][0]
                log_p = log_prob[0][new_k].item()

                node = BeamSearchNode(decoder_hidden, n, decoded_t, n.logp + log_p, n.leng + 1)
                # print((log_p,decoded_t))
                score = -node.eval()
                nextnodes.append((score, node))

            # put them into queue
            for i in range(len(nextnodes)):
                score, nn = nextnodes[i]
                # print((score, nn))
                nodes.put((score, nn))
                # increase qsize
            qsize += len(nextnodes) - 1
            
        # choose nbest paths, back trace them
        if len(endnodes) == 0:
            endnodes = [nodes.get() for _ in range(topk)]

        utterances = []
        for score, n in sorted(endnodes, key=operator.itemgetter(0)):
            utterance = []
            utterance.append(n.wordid)
            # back trace
            while n.prevNode != None:
                n = n.prevNode
                utterance.append(n.wordid)

            utterance = utterance[::-1]
            utterances.append(utterance)

        decoded_batch.append(utterances)

    return decoded_batch

"""### Merging experiment"""

from rdkit import Chem

import moses
import numpy as np
import pandas as pd

# train = moses.get_dataset()

# mdf = pd.DataFrame(train, columns=["Smiles"])

# mdf["Length"] = mdf.Smiles.str.len()

# pd.options.display.float_format = '{:,.2f}'.format

# mdf.describe()

from tqdm.auto import tqdm

def enumerate_SMILES(smi):
  mol = Chem.MolFromSmiles(smi)
  smis = []
  for i in range(200):
      smis.append(Chem.MolToSmiles(mol,doRandom=True,canonical=False))
  return list(set(smis))

def construct_pairs(set1, set2):
  return [ (smi1, smi2) for smi1 in set1 for smi2 in set2 ]


def generate_averaged_mols(s, model, op='mean', weights = [], reward = 0.5):
    assert len(s) == len(weights)

    src_list= []
    longest_str = max(s, key=(lambda x : len(x)))
    s = SRC.pad(s)
    product_name = longest_str
    example = data.Example.fromlist([product_name, ''], 
                                fields=[('src', SRC), ('trg', TGT)])
    src = torch.LongTensor([list(SRC.vocab.stoi[s] for s in example.src)])
    
    #Normalize weights so that they sum up to 1
    weights = [x/sum(weights) for x in weights]

    for c in s:
      product_name = c
      example = data.Example.fromlist([product_name, ''], 
                                  fields=[('src', SRC), ('trg', TGT)])
      src = torch.LongTensor([list(SRC.vocab.stoi[s] for s in example.src)])

      src = src.cuda()

      src_mask = (src != SRC.vocab.stoi["<blank>"]).unsqueeze(-2)
      src_mask = src_mask.cuda()
      memory = model.encode(src, src_mask)
      # print(np.linalg.norm(memory.squeeze().cpu().detach().numpy()))
      src_list.append(memory)
    
    # src = torch.LongTensor([list(SRC.vocab.stoi[s] for s in longest_str)])
    # src = src.cuda()
    # src_mask = (src != SRC.vocab.stoi["<blank>"]).unsqueeze(-2)
    # src_mask = src_mask.cuda()
    #mean
    if op == 'mean':
      memory = torch.mean(torch.stack([mem * weights[idx] for idx, mem in enumerate(src_list)]).squeeze(1), dim=0).unsqueeze(0)
    #sum
    elif op == 'sum':
      memory = torch.sum(torch.stack(src_list).squeeze(1), dim=0).unsqueeze(0)
    #Subtraction
    elif op == 'sub':
      memory = torch.sub(src_list[0], src_list[1])
    else:
      raise ValueError()
    # print(memory.size())
    out = beam_decode(model, src, src_mask, 
                        max_len=60, start_symbol=TGT.vocab.stoi["<s>"], stochastic=False, memory = memory, topk = 50, beam_width=4, give_up_limit=50000, length_reward=reward)    
    # # # print("Source:", end="\t")
    # # # print(product_name)
    # # # print("Generated:", end="\t")
    # # # print(out)
    generated = set([ "".join([TGT.vocab.itos[x.item()] for x in z][1:-1]) for z in out[0]])
    # # print()
    # for i in range(1, out.size(1)):
    #     sym = TGT.vocab.itos[out[0, i]]
    #     if sym == "</s>": break
    #     print(sym, end ="")
    # print()
    # frob = np.linalg.norm(memory.squeeze().cpu().detach().numpy())
    return generated
    
PATH = 'model/lc_epoch_16_augmented_multi.pt'
model = make_model(len(SRC.vocab), len(TGT.vocab), N=6)
model = model.cuda()
model.load_state_dict(torch.load(PATH))

model.eval()

ex1 = ['CCOCC1CC1(Br)Br', "Fc1nccc(Br)c1Br"]
ex2 = ['CCCS(=O)c1ccc2[nH]c(=NC(=O)OC)[nH]c2c1', "Fc1nccc(Br)c1Br"]
ex3 = ['CCCS(=O)c1ccc2[nH]c(=NC(=O)OC)[nH]c2c1', 'CC(C)(C)C(=O)C(Oc1ccc(Cl)cc1)n1ccnc1']

for idx, ex in enumerate([ex3], start=2):
    smi1 = ex[0]
    smi2 = ex[1]

    smi_set1 = enumerate_SMILES(smi1)
    smi_set2 = enumerate_SMILES(smi2)

    pairs = construct_pairs(smi_set1, smi_set2)
    np.random.shuffle(pairs)

    prob_weight1 = [ [0.55, 0.45], [0.65, 0.35], [0.75, 0.25], [.85, .15], [.95, .5]]
    prob_weight2 = [ list(reversed(x)) for x in prob_weight1]

    prob_weights = prob_weight1 + prob_weight2
    for weight in tqdm(prob_weights):
        for reward in [0.8, 0.5, 0.3]:
            mols_trials = []
            for smi_pair in tqdm(pairs[:100]):
                generated = generate_averaged_mols(list(smi_pair), model, weights=[weight[0], weight[1]], reward=reward)
                mols_trials.append(generated)

            from moses.metrics import remove_invalid

            df_rows = []
            for trial in mols_trials:
                valid = remove_invalid(trial)
                df_rows.append( [ trial, len(trial), valid, len(valid) ] )

            import pandas as pd

            df1 = pd.DataFrame(df_rows, columns=['Generated', 'len_generated', 'valid', 'len_valid'])
            df1.to_csv(f"{str(idx)}_weights_{str(weight[0])}_{str(weight[1])}_sampling_{str(reward)}_test_gen.csv")


"""### End"""

