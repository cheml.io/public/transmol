# Transmol: Repurposing Language Model for Molecular Generation

Recent advances in convolutional neural networks have inspired the application of deep learning to other disciplines. Even though image processing and natural language processing have turned out to be the most successful, there are many other domains that have also benefited. Among them, life sciences in general and chemistry and drug design in particular. In line with that, from 2018 the scientific community has seen a surge of methodologies related to the generation of diverse molecular libraries using machine learning. However to date, attention mechanisms have not been employed for the problem of *de novo* molecular generation. Here we employ a variant of transformers, an architecture recently developed for natural language processing, for this purpose. Our results indicate that this adapted Transmol model is indeed applicable for the task of generating molecular libraries and leads to statistically significant increases in some of the core metrics of the MOSES benchmark. The presented model can be tuned to either input-guided or diversity-driven generation modes by applying standard one-seed and novel two-seed approaches, respectively. Accordingly, the one-seed approach is best suited for the targeted generation of focused libraries composed of close analogues of the seed structure, while the two-seed approach allows to dive deeper into under-explored regions of the chemical space by attempting to generate the molecules that resemble a mixture of both seeds. To further benefit the chemical community, the Transmol algorithm has been incorporated into our [cheML.io](https://cheml.io) web database of ML-generated molecules as a second generation on-demand methodology.

__For more details, please refer to the [paper](https://doi.org/10.26434/chemrxiv.14350610.v1).__

If you are using Transmol in your research paper, please cite us as

```
@article{10.26434/chemrxiv.14350610.v1,
  title={Transmol: Repurposing Language Model for Molecular Generation},
  journal={ChemRxiv}, 
  publisher={Cambridge Open Engage}, 
  author={Zhumagambetov, Rustam and Peshkov, Vsevolod A. and Fazli, Siamac}, 
  year={2021}
}
```

![pipeline](images/abstract.png)

# Installation

## Conda environment
Install dependencies using conda.

1. Clone the repository:
```bash
git lfs install
git clone https://gitlab.com/cheml.io/public/transmol.git
```

2. Install conda environment:
```bash
conda env create -f environment.yml
conda activate transformer-r
```

3. Unzip dataset files
```bash
bash unzip.sh
```

# Training

```bash
python train.py
```


# Generation

```bash
python sample1.py
```

or for 2 seed generation

```bash
python sample2.py
```

